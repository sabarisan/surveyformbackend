import express from 'express';
const app = express();
import mongoose from 'mongoose';
import formsRouter from './forms/form.router';
import { userRouter } from './user/user.router';
import { devConfig } from './env/development';

import { setGlobalMiddleware } from './middleware/global-middleware';

var PORT = devConfig.port;
app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`);
});
// registered Global middleware
setGlobalMiddleware(app);
app.use('/forms', formsRouter);
app.use('/users', userRouter);

mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

// DB server connectivity
mongoose.connect(`mongodb://localhost/${devConfig.database}`, (err) => {
    if (err) { console.log('DB not connected') } else {
        console.log('DB connected successfully');
    }
    // console.log('Sorry Unable to Connect');
});
