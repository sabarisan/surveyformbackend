import userService from "./user.service";
import bcryptjs from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { BAD_REQUEST, INTERNAL_SERVER_ERROR, UNAUTHORIZED } from "http-status-codes";
import User from './user.model';
import { devConfig } from "../env/development";


export default {
    async signup(req, res) {
        try {

            // validate the reuest 
            const { error, value } = userService.validateSchema(req.body);

            if (error && error.details) {
                return res.status(BAD_REQUEST).json(error)
            }

            // encrypt the user password

            // create ne user
            const user = await User.create(value);
            return res.json({ success: true, message: 'User Created Successfully' });
        } catch (err) {
            console.log(err);
            return res.status(INTERNAL_SERVER_ERROR).json(err);
        }
    },

    async login(req, res) {
        try {
            const { error, value } = userService.validateSchema(req.body);

            if (error && error.details) {
                return res.status(BAD_REQUEST).json(error)
            }

            const user = await User.findOne({ email: value.email });
            if (!user) {
                return res.status(BAD_REQUEST).json({ err: 'Invalid email or password' });
            }
            const matched = await bcryptjs.compare(value.password, user.password)
            if (!matched) {
                return res.status(UNAUTHORIZED).json({ err: 'Invalid Credentails' });
            }
            const token = jwt.sign({ id: user._id }, devConfig.secret, { expiresIn: '1d' });

            return res.json({ success: true, token });

        } catch (err) {
            return res.status(INTERNAL_SERVER_ERROR).json(err);
        }
    },

    async test(req, res) {
        return res.json(req.user);
    }

}